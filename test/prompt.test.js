import generatePrompt from "../scriptPrompt/prompt";

test('test', () => {
  const prompt = generatePrompt('Male','Elf','Archer','25')
  expect(prompt).toBe(`create a dungeon and dragon character in realistic rendering with a detailed style and with a focus on the character's bust , is 25 years old, he is a Male with a beard and a very strong physique and  and her race is an elf has a slender figure, fair and smooth skin, almond-shaped eyes, pointed ears, and long fine hair and this character is a ranger with a big bow and quiver  `)
});
