var express = require('express');


function generatePromptGender(gender) {
    switch (gender) {
        case "Female":
            console.log()
            var promptgender = "she is a " + gender + " with a long hair and a very strong physique ";
            return promptgender;

        case "Male":
            var promptgender = "he is a " + gender + " with a beard and a very strong physique ";
            return promptgender;
        default:
            var promptgender = ""
            return promptgender
    }
    return promptgender;
}

function generatePhysique(physique){
    switch (physique) {
        case "one_eyed":
            var promptgender = "and lost an eye ";
            return promptgender;
        case "burned":
            var promptgender = "and have multiples burn marks ";
            return promptgender;
        case "scar":
            var promptgender = "and have a lot of scars ";
            return promptgender;
        case "wooden_leg":
            var promptgender = "and have a wooden leg ";
            return promptgender;
        default:
            var promptgender = ""
            return promptgender
    }
    return promptgender;
}


function generatePromptRace(race) {
    switch (race) {
        case "Human":
            var promptrace = " and her race is a human has a medium build, a fair complexion, and a broad nose. They also have brown or black hair, brown or blue eyes, with a Caucasian style ";
            return promptrace;

        case "Elf":
            var promptrace = " and her race is an elf has a slender figure, fair and smooth skin, almond-shaped eyes, a very pointed ears, and long fine hair ";
            return promptrace;

        case "Dwarf":
            var promptrace = " Dwarves in World of Warcraft are a robust and courageous warrior race originating from the holy mountain of Khaz Modan in the fictional world of Azeroth. They are known for their endurance, determination, and talent for exploration and discovery. Dwarves have a rich and complex culture based on tradition, honor, and loyalty, and are skilled in underground warfare. They are formidable fighters armed with axes, hammers, and crossbows. In summary, dwarves in World of Warcraft are proud warriors with a rich culture and a talent for exploration and discovery and ";
            return promptrace;

        case "Goblin":
            var promptrace = " and her race is a goblin has a small and wiry frame, with green or brownish-gray skin, beady eyes, a large nose, and pointed ears. They also have sharp teeth, long fingers, and a mischievous grin ";
            return promptrace;
        default:
            var promptrace = ""
            return promptrace
    }
    return promptrace;
}

function generatePromptClass(characterClass) {
    switch (characterClass) {
        case "Warrior":
            var promptclass = "holds a heavy sword ";
            return promptclass;

        case "Mage":
            var promptclass = "holds a magic staff ";
            return promptclass;

        case "Rogue":
            var promptclass = "holds a dagger ";
            return promptclass;

        case "Berserker":
            var promptclass = "holds a heavy sword with a rage face, red eyes";
            return promptclass;

        case "Priest":
            var promptclass = "holds a bible  ";
            return promptclass;

        case "Knight":
            var promptclass = "holds a heavy sword and armor ";
            return promptclass;

        case "Archer":
            var promptclass = " ,holds a bow, is aiming a bow ";
            return promptclass;
        default:
            var promptclass = ""
            return promptclass
    }
    return promptclass
}

function generatePrompt(gender, race, characterClass, age, physique) {
    if (race === "Goblin") {
        if (gender === "Male"){
            return "cinematic concept art of an Goblin from \"harry Potter\" , 8k hd dof, portrait realistic, unreal engine , RTX quality, half body, front photo with tof effect,with a fairly discreet slightly blurred background image that is either on a sunset or midday , is " + age + " years old, " + generatePromptClass(characterClass) + generatePhysique(physique);
        }
        else{
            return "cinematic concept art of an female Goblin from \"harry Potter\" , 8k hd dof, portrait realistic, unreal engine , RTX quality, half body, front photo with tof effect,with a fairly discreet slightly blurred background image that is either on a sunset or midday , is " + age + " years old, " + generatePromptClass(characterClass) + generatePhysique(physique);

        }

    }
    if (race === "Elf") {
        if (gender === 'Male'){
            return "cinematic concept art of an elf like \"Legolas\" from \"lords of ring\" , 8k hd dof, portrait realistic, unreal engine , RTX quality,half body, front photo with tof effect,with a fairly discreet slightly blurred background image that is either on a sunset or midday , is " + age + " years old, " + generatePromptClass(characterClass) + generatePhysique(physique);
        }
        else{
            return "cinematic concept art of an female elf like \"Legolas\" from \"lords of ring\" , 8k hd dof, portrait realistic, unreal engine , RTX quality,half body, front photo with tof effect,with a fairly discreet slightly blurred background image that is either on a sunset or midday , is " + age + " years old, " + generatePromptClass(characterClass) + generatePhysique(physique);

        }
    }
    if (race === "Dwarf") {
        if (gender === "Male"){
            return "cinematic concept art of an Dwarf like \"Gimli\" from \"lords of ring\" , 8k hd dof, portrait realistic, unreal engine, RTX quality, half body, front photo with tof effect,with a fairly discreet slightly blurred background image that is either on a sunset or midday , is " + age + " years old, " + generatePromptClass(characterClass) + generatePhysique(physique);

        }
        else{
            return "cinematic concept art of an female Dwarf from \"world of warcraft\" , 8k hd dof, portrait realistic, unreal engine, RTX quality, half body, front photo with tof effect,with a fairly discreet slightly blurred background image that is either on a sunset or midday , is " + age + " years old, " + generatePromptClass(characterClass) + generatePhysique(physique);
        }

    }
    if (race === "Human") {
        if (gender === "Male"){
            console.log("Male")
            return "cinematic concept art of an man like \"Aragorn\" from \"lords of ring\", 8k hd dof, portrait realistic, unreal engine, RTX quality, half body, front photo with tof effect,with a fairly discreet slightly blurred background image that is either on a sunset or midday , is " + age + " years old, " + generatePromptClass(characterClass) + generatePhysique(physique);
        }
        else{
            console.log("female")
            return "cinematic concept art of an woman like \"Éowyn\" from \"lords of ring\", 8k hd dof, portrait realistic, unreal engine, RTX quality, half body, front photo with tof effect,with a fairly discreet slightly blurred background image that is either on a sunset or midday , is " + age + " years old, " + generatePromptClass(characterClass) + generatePhysique(physique);
        }

    }
}


module.exports = generatePrompt;
