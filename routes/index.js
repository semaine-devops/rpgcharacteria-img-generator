var express = require('express');
var router = express.Router();
const jwt = require('jsonwebtoken');
require('dotenv').config()
var openAi = require('openai')
const generatePrompt = require("../scriptPrompt/prompt");
const configuration = new openAi.Configuration({
    apiKey: process.env.OPENAI_API_KEY,
});
const openai = new openAi.OpenAIApi(configuration);

/* GET home page. */
router.post('/generate', async function (req, res, next) {
    console.log('background generator')
    const jwtToken = req.body.jwtToken
    const verify = jwt.verify(jwtToken, process.env.SECRET)
    if (!verify) {
        res.status(401).json({
            error: {
                message: "Unauthorized",
            }
        });
        return;
    }

    if (!configuration.apiKey) {
        res.status(500).json({
            error: {
                message: "OpenAI API key not configured, please follow instructions in README.md",
            }
        });
        return;
    }
    const gender = req.body.gender || '';
    if (gender.trim().length === 0) {
        res.status(400).json({
            error: {
                message: "Please enter a valid gender",
            }
        });
        return;
    }
    const age = req.body.age || '';
    if (age.trim().length === 0) {
        res.status(400).json({
            error: {
                message: "Please enter a valid age",
            }
        });
        return;
    }
    const characterClass = req.body.characterClass || '';
    if (characterClass.trim().length === 0) {
        res.status(400).json({
            error: {
                message: "Please enter a valid class",
            }
        });
        return;
    }
    const physique = req.body.physique || '';
    if (characterClass.trim().length === 0) {
        res.status(400).json({
            error: {
                message: "Please enter a valid class",
            }
        });
        return;
    }
    const race = req.body.race || '';
    if (race.trim().length === 0) {
        res.status(400).json({
            error: {
                message: "Please enter a valid race",
            }
        });
        return;
    }

    const prompt = generatePrompt(gender, race, characterClass, age, physique);
    console.log(prompt);

    try {
        const response = await openai.createImage({
            prompt: prompt,
            //prompt: "Rendu 3D d'un portrait focalisé d'" + genderSentence + characterClass + " " + race + " de "  + age + " ans de Donjons et Dragons",
            n: 1,
            size: "256x256",
        });
        const image_url = response.data.data[0].url;
        console.log(image_url)
        res.status(200).json({
            result: image_url
        });
    } catch (error) {
        if (error.response) {
            console.error(error.response.status, error.response.data);
            res.status(error.response.status).json(error.response.data);
        } else {
            console.error(`Error with OpenAI API request: ${error.message}`);
            res.status(500).json({
                error: {
                    message: 'An error occurred during your request.',
                }
            });
        }
    }
});

module.exports = router;
